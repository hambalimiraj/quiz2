<?php
function perolehan_medali($soal)
{
    $hasil = [
        ["negara" => "Indonesia", "emas" => 0, "perak" => 0, "perunggu" => 0],
        ["negara" => "India", "emas" => 0, "perak" => 0, "perunggu" => 0],
        ["negara" => "Korea Selatan", "emas" => 0, "perak" => 0, "perunggu" => 0],
    ];
    if (!$soal) {
        return "no data";
    }
    for ($i = 0; $i < count($soal); $i++) {
        for ($j = 0; $j < count($hasil); $j++) {
            if ($soal[$i][0] == $hasil[$j]["negara"]) {
                $hasil[$j][$soal[$i][1]]++;
            }
        }
    }
    return print_r($hasil);
}

perolehan_medali(
    array(
        array('Indonesia', 'emas'),
        array('India', 'perak'),
        array('Korea Selatan', 'emas'),
        array('India', 'perak'),
        array('India', 'emas'),
        array('Indonesia', 'perak'),
        array('Indonesia', 'emas'),
    )
);